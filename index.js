/*
    Welcome to JSONSave
    Copyright (c) 2017-2020 TStudios. All Rights Reserved.
*/

module.exports = function(dataname) {
    dataname=dataname || "data"

    let returnv = {}
    
    global[dataname] = {}

    try {
        fs.accessSync("./data");
    } catch (e) {
        fs.mkdirSync("./data");
    }

    global.LoadData = function(server_id) {
        if(!global[dataname][server_id]) {
            global[dataname][server_id]={}
        }
        fs.stat("./data/" + server_id + ".json", function(err,stat) { 
            if (err == null) { 
                global[dataname][server_id] = require("./data/" + server_id + ".json")
            } else if(err && err.code === 'ENOENT') {
                global[dataname][server_id] = {}
                fs.writeFile('./data/' + server_id + ".json","{}", function() {
            
                })
            } else {
                error_log("An error has occured while getting file:\n" + e)
                process.exit(5)
            }
        });
        return global[dataname][server_id]
    }
    global.EnsureData = function(server_id) {
        fs.stat("./data/" + server_id + ".json", function(err,stat) {
            if(err && err.code === 'ENOENT') {
                LoadData(server_id)
            } else {
                if (typeof(JSON.stringify(global[dataname][server_id])) == "undefined" || JSON.stringify(global[dataname][server_id]) == undefined) {
                    try {
                        data[server_id] = require("./data/"+server_id+".json")                    
                    } catch (error) {
                        
                    }
                }
            }
        })
        if (typeof(JSON.stringify(global[dataname][server_id])) == "undefined" || JSON.stringify(global[dataname][server_id]) == undefined) {
            global[dataname][server_id]={}
        }
        /*if(typeof(data[server_id]) == 'undefined') {
            LoadData(server_id)
        }*/
    }
    global.SaveData = function(server_id) {
        EnsureData(server_id)
        if (typeof(global[dataname][server_id]) != "object") {
            error_log("Data is not an object!")
            process.exit(90)
        }
        fs.writeFile('./data/' + server_id + ".json",JSON.stringify(global[dataname][server_id]), function() {

        })
        fs.writeFile('./data/complete.json',JSON.stringify(global[dataname]), function() {

        })
    }
    return returnv
}
